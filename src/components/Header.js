const Header = () => {
  return (
    <header class="header-area header-sticky">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <nav class="main-nav">
              <a href="/" class="logo">
                <img
                  src="assets/images/logo.png"
                  width="1000"
                  height="169"
                  alt=""
                />
              </a>
              <ul class="nav">
                <li class="submenu">
                  <a href="about-us.html">About us</a>
                  <ul>
                    <li>
                      <a href="">Our Team</a>
                    </li>
                    <li>
                      <a href="">Vision</a>
                    </li>
                    <li>
                      <a href="">Mission</a>
                    </li>
                    <li>
                      <a href="">Goal</a>
                    </li>
                  </ul>
                </li>
                <li class="">
                  <a href="b2b-news.html">B2B News</a>
                </li>
                <li class="">
                  <a href="opportunities.html">Opportunities</a>
                </li>
                <li class="">
                  <a href="e-learning.html"> E-learning</a>
                </li>
                <li class="">
                  <a href="networking.html"> Networking</a>
                </li>
                <li class="">
                  <a href="tips-discussions.html"> Tips & Discussions </a>
                </li>
                <li class="">
                  <a href="partners.html"> Partners</a>
                </li>
                <li class="">
                  <a href="search.html">
                    <i class="fa fa-fw fa-search"></i>
                  </a>
                </li>
                <li>
                  <a href="/login">
                    <i class="fa fa-fw fa-user"></i> Login
                  </a>
                </li>
                <li>
                  <a
                    href="register.html"
                    class="get-started"
                    style={{ color: "#fff!important" }}
                  >
                    Get Started
                  </a>
                </li>
              </ul>
              <a class="menu-trigger">
                <span>Menu</span>
              </a>
            </nav>
          </div>
        </div>
      </div>
    </header>
  );
};
export default Header;
