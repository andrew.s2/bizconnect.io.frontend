import Header from "./Header";
import Footer from "./Footer";
import Partners from "./Partners";
import PropTypes from "prop-types";

const Search = ({ history }) => {
  const handleSubmit = () => {
    history.push("/detail");
  };
  return (
    <>
      <Header></Header>
      <div class="welcome-area" id="welcome">
        <div class="welcome-bg">
          <div class="header-text">
            <div class="container">
              <div class="row">
                <div
                  class="left-text col-lg-8 col-md-6 col-sm-12 col-xs-12"
                  data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
                >
                  <h1>A Global B2B Search Engine And Community</h1>
                  <form class="main-search" onSubmit={handleSubmit}>
                    <input type="text" placeholder="Search..." name="search" />
                    <button type="submit">
                      <i class="fa fa-search"></i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Partners></Partners>
      <Footer></Footer>
    </>
  );
};
Search.propTypes = {
  history: PropTypes.object,
};
export default Search;
