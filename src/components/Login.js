import Footer from "./Footer";
import PropTypes from "prop-types";

const Login = ({ history }) => {
  const handleSubmit = () => {
    history.push("/search");
  };
  return (
    <>
      <div id="login-area">
        <div class="container">
          <div class="row">
            <div
              class="login-text-panel left-text col-lg-8 col-md-8 col-sm-12 col-xs-12"
              data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
            >
              <div class="header-text">
                <h1>A Global B2B Search Engine And Community</h1>
                <p class="sub-txt">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
                <div>
                  <img
                    src="assets/images/left-image.png"
                    width="667"
                    height="498"
                    alt=""
                    class="rounded img-fluid d-block mx-auto"
                  />
                </div>
              </div>
            </div>
            <div
              class="login-form-panel-bg col-lg-4 col-md-4 col-sm-12 col-xs-12"
              data-scroll-reveal="enter left move 30px over 0.6s after 0.4s"
            >
              <div class="container">
                <div class="logo">
                  <a href="/">
                    <img
                      src="assets/images/logo.png"
                      width="1000"
                      height="169"
                      alt=""
                    />
                  </a>
                </div>
                <h5>Global B2B Search Engine</h5>
                <form onSubmit={handleSubmit}>
                  <label for="uname">
                    <b>Username</b>
                  </label>
                  <input
                    type="text"
                    class="text-input"
                    placeholder="Enter Username"
                    name="uname"
                    required
                  />

                  <label for="psw">
                    <b>Password</b>
                  </label>
                  <input
                    type="password"
                    class="text-input"
                    placeholder="Enter Password"
                    name="psw"
                    required
                  />
                  <p>
                    <a href="#">Forget Password?</a>
                  </p>
                  <button type="submit">Sign In</button>
                  <p style={{ textAlign: "center" }}>
                    Do not have a business account yet?
                    <br /> <a href="register.html">Register</a> now
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

Login.propTypes = {
  history: PropTypes.object,
};
export default Login;
